import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public loader = false;
  public view = 'form';
  registrationForm = this.fb.group({
    fullname: ['', [Validators.required]],
    gender: ['', [Validators.required]],
    orgnaization: [''],
    position: ['', [Validators.required]],
    telephone: ['', [Validators.required]],
    email: ['', [Validators.required]],
    twitter: [''],
    instagram: [''],
    facebook: [''],
    tshirt: [''],
    allergies: [''],
    code: ['']
  });

  constructor(
    private fb: FormBuilder,
    private registerService: RegisterService
  ) { }

  title = 'Transform Summit';

  changeGender(id: string) {
    this.registrationForm.patchValue({
      gender: id
    });
  }

  changePosition(selected: number) {
    this.registrationForm.patchValue({
      position: selected
    });
  }

  changeSize(size: number) {
    this.registrationForm.patchValue({
      tshirt: size
    });
  }

  submit() {
    if (this.registrationForm.status === 'INVALID') {
      console.log('Errors exist');
      return;
    }
    this.loader = true;
    this.registerService.register(this.registrationForm.value)
      .subscribe(res => {
        if (res === true) {
          this.loader = false;
          this.view = 'success';
        } else {
          alert('Failed');
        }
      });
  }
}
