import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private apiService: ApiService
  ) { }

  public register(details): Observable<any> {
    return this.apiService.post('/register', {
      fullname: details.fullname,
      gender: details.gender,
      orgnaization: details.orgnaization,
      position: details.position,
      telephone: details.telephone,
      email: details.email,
      twitter: details.twitter,
      instagram: details.instagram,
      facebook: details.facebook,
      tshirt: details.tshirt,
      allergies: details.allergies,
      code: details.code
    }).pipe(map(
      data => {
        const stringy = JSON.stringify(data);
        const parsed = JSON.parse(stringy);
        if (parsed.status === 'success') {
          return true;
        }
        return parsed;
      }));
  }
}
